# -*- coding: utf-8 -*-
import json
import codecs
import time
import re
import jpype

start = time.time()
f = codecs.open('../origin/news_orig.txt', 'r', 'utf-8')
f1 = codecs.open('../benchmark/news.txt', 'r', 'utf-8')
f2 = codecs.open('./NewsHanlp.json','w', 'utf-8')
# 按portable版jar包存放位置加载jvm
jpype.startJVM(jpype.getDefaultJVMPath(), '-Djava.class.path=D:\Users\hanlp\hanlp-portable-1.3.2.jar;D:\Users\hanlp',"-Xms512m", "-Xmx512m")
HanLP = jpype.JClass('com.hankcs.hanlp.HanLP')
i,j,m,n = 0,0,0,0
r1 = re.compile(ur'[^0-9a-zA-Z\u4E00-\u9FA5]+',re.U)
for line in f:
    line = re.sub(r1, '', line).strip()
    list_str = HanLP.segment(line.encode('utf8')).toString()
    # 所得字符串为[商品/n, 和/cc, 服务/vn]，将其转化为不含词性的列表
    words_list = []
    temp_str = ''
    for s in list_str:
        if s == '/':
            words_list.append(temp_str)
            temp_str = ''
        elif s == ',':
            temp_str = ''
        elif s == '['or s == ' ':
            pass
        else:
            temp_str+=s
    json.dump(words_list, f2, ensure_ascii=False)
    f2.write('\n')
    result = []
    tk = 0
    for t in words_list:
        result.append([tk, tk+len(t)])
        tk+=len(t)
    # 答案分词位置
    answer = []
    k = 0
    line1 = f1.readline().split()
    for s in line1:
        answer.append([k, k+len(s)])
        k+=len(s)
    # 忽略标点导致错位情况
    if k != tk:
        continue
    json.dump(result, f2, ensure_ascii=False)
    f2.write('\n')
    json.dump(answer, f2, ensure_ascii=False)
    f2.write('\n')
    # 计算准确率和召回率
#    i, j = 0, 0
    for r in result:
        if r in answer:
            i, j = i+1, j+1
        else:
            i, j = i, j+1
#    m, n =0, 0
    for a in answer:
        if a in result:
            m, n = m+1, n+1
        else:
            m, n = m, n+1
    p = float(i)/j
    precision = 'precision = {0}/{1} = {2}\n'.format(str(i), str(j), str(p))
    f2.write(precision)
    r = float(m)/n
    recall = 'recall = {0}/{1} = {2}\n'.format(str(m), str(n), str(r))
    f2.write(recall)
end = time.time()
jpype.shutdownJVM()
f2.write('f1 = ' + str(2 * p * r / (p+ r)) + '\n')
f2.write('time: '+ str(end-start)+ ' seconds\n')
f.close()
f1.close()
f2.close()

