# -*- coding: utf-8 -*-
import jieba
import json
import codecs
import re

f = codecs.open('../origin/auto_comments_orig.txt', 'r', 'utf-8')
f1 = codecs.open('../benchmark/auto_comments.txt', 'r', 'utf-8')
f2 = codecs.open('./AutoJieba.json','w')
r1 = re.compile(ur'[^0-9a-zA-Z\u4E00-\u9FA5]+',re.U)
i,j,m,n = 0,0,0,0
for line in f:
    # 结巴分词位置
    result = []
    line = re.sub(r1, '', line).strip()
    resultList = list(jieba.cut(line))
    json.dump(resultList, f2, ensure_ascii=False)
    f2.write('\n')
    cutResult = jieba.tokenize(line)
    for tk in cutResult:
        result.append([tk[1],tk[2]])
    # 答案分词位置
    answer = []
    k = 0
    line1 = f1.readline().split()
    for s in line1:
        answer.append([k, k+len(s)])
        k+=len(s)
    # 忽略标点导致错位情况
    if k != tk[2]:
        continue
    json.dump(result, f2, ensure_ascii=False)
    f2.write('\n')
    json.dump(answer, f2, ensure_ascii=False)
    f2.write('\n')
    # 计算准确率和召回率
#    i, j = 0, 0
    for r in result:
        if r in answer:
            i, j = i+1, j+1
        else:
            i, j = i, j+1
#    m, n =0, 0
    for a in answer:
        if a in result:
            m, n = m+1, n+1
        else:
            m, n = m, n+1
    p = float(i)/j
    precision = 'precision = {0}/{1} = {2}\n'.format(str(i), str(j), str(p))
    f2.write(precision)
    r = float(m)/n
    recall = 'recall = {0}/{1} = {2}\n'.format(str(m), str(n), str(r))
    f2.write(recall)
end = time.time()
f2.write('f1 = ' + str(2 * p * r / (p+ r)) + '\n')
f2.write('time: '+ str(end-start)+ ' seconds\n')
f.close()
f1.close()
f2.close()