# -*- coding:utf-8 -*-
from jpype import *

startJVM(getDefaultJVMPath(), '-Djava.class.path=D:\Users\hanlp\hanlp-portable-1.3.2.jar;D:\Users\hanlp', "-Xms512m",
         "-Xmx512m")
HanLP = JClass('com.hankcs.hanlp.HanLP')
content = ("程序员(英文Programmer)是从事程序开发、维护的专业人员。"
           "一般将程序员分为程序设计人员和程序编码人员，但两者的界限并不非常清楚，特别是在中国。"
           "软件从业人员分为初级程序员、高级程序员、系统分析员和项目经理四大类。")
print(HanLP.extractKeyword(content, 10).toString())
content = ("此外，公司拟对全资子公司吉林欧亚置业有限公司增资4.3亿元，"
           "增资后，吉林欧亚置业注册资本由7000万元增加到5亿元。"
           "吉林欧亚置业主要经营范围为房地产开发及百货零售等业务。"
           "目前在建吉林欧亚城市商业综合体项目。2013年，实现营业收入0万元，实现净利润-139.13万元。")
print(HanLP.extractKeyword(content, 10).toString())
document = "水利部水资源司司长陈明忠9月29日在国务院新闻办举行的新闻发布会上透露，" \
           "根据刚刚完成了水资源管理制度的考核，有部分省接近了红线的指标，" \
           "有部分省超过红线的指标。对一些超过红线的地方，陈明忠表示，对一些取用水项目进行区域的限批，" \
           "严格地进行水资源论证和取水许可的批准。"
print(HanLP.extractKeyword(document, 10).toString())
testCases = [
    "永和服装饰品有限公司",
    "结婚的和尚未结婚的确实在干扰分词啊",
    "买水果然后来世博园最后去世博会",
    "中国的首都是北京",
    "欢迎新老师生前来就餐",
    "工信处女干事每月经过下属科室都要亲口交代24口交换机等技术性器件的安装工作",
    "随着页游兴起到现在的页游繁盛，依赖于存档进行逻辑判断的设计减少了，但这块也不能完全忽略掉。"]
for sentence in testCases:
    print(HanLP.extractKeyword(sentence, 10).toString())
shutdownJVM()
