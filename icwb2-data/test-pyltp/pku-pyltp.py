# -*- coding: utf-8 -*-
import json
import codecs
import pyltp
import time

start = time.time()
f = codecs.open('../testing/pku_test.utf8', 'r', 'utf8')
f1 = codecs.open('../gold/pku_test_gold.utf8', 'r', 'utf8')
f2 = codecs.open('./pku-pyltp.json','w', 'utf8')
i,j,m,n = 0,0,0,0
segmentor = pyltp.Segmentor()
segmentor.load('D:\\BaiduYunDownload\\ltp_data\\cws.model')
for line in f:
    result = []
    line = line.strip()
    words = segmentor.segment(line.encode('utf8'))
    words_list = [word.decode('utf8') for word in list(words)]
    json.dump(words_list, f2, ensure_ascii=False)
    f2.write('\n')
    result = []
    tk = 0
    for t in words_list:
        result.append([tk, tk+len(t)])
        tk+=len(t)
    #答案分词位置
    answer = []
    k = 0
    line1 = f1.readline().split()
    for s in line1:
        answer.append([k, k+len(s)])
        k+=len(s)
    # 忽略标点导致错位情况
    if k != tk:
        continue
    json.dump(result, f2, ensure_ascii=False)
    f2.write('\n')
    json.dump(answer, f2, ensure_ascii=False)
    f2.write('\n')
    #计算准确率和召回率
    for r in result:
        if r in answer:
            i, j = i+1, j+1
        else:
            i, j = i, j+1
    for a in answer:
        if a in result:
            m, n = m+1, n+1
        else:
            m, n = m, n+1
    p = float(i)/j
    precision = 'precision = {0}/{1} = {2}\n'.format(str(i), str(j), str(p))
    f2.write(precision)
    r = float(m)/n
    recall = 'recall = {0}/{1} = {2}\n'.format(str(m), str(n), str(r))
    f2.write(recall)
end = time.time()
segmentor.release()
f2.write('f1 = ' + str(2 * p * r / (p+ r)) + '\n')
f2.write('time: '+ str(end-start)+ ' seconds\n')
f.close()
f1.close()
f2.close()
